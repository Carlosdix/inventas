package contabilidad.inventas.models.services;

import contabilidad.inventas.models.Producto;
import org.springframework.stereotype.Component;

import java.util.List;

public interface IProductoService
{
    List<Producto> findAll();

    Producto findById(String pID);

    Producto save(Producto producto);

    void delete(String pID);
}
