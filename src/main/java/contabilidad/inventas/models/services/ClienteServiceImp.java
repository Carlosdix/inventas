package contabilidad.inventas.models.services;

import contabilidad.inventas.models.Producto;
import contabilidad.inventas.models.dao.IProductoDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class ClienteServiceImp implements IProductoService
{
    private IProductoDao dao;

    @Override
    @Transactional(readOnly = true)
    public List<Producto> findAll()
    {
        return (List<Producto>) dao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Producto findById(String pID)
    {
        return dao.findById( pID ).orElse( null );
    }

    @Override
    @Transactional
    public Producto save(Producto producto)
    {
        return dao.save(producto);
    }

    @Override
    @Transactional
    public void delete(String pID)
    {
        dao.deleteById( pID );
    }
}
