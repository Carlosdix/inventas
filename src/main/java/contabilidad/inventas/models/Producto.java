package contabilidad.inventas.models;

import javax.persistence.*;

@Entity
@Table(name = "productos")
public class Producto
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    private String nombre;

    private double precio;

    private boolean estado;

    public Producto(String id, String nombre, double precio, boolean estado)
    {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.estado = estado;
    }

    public Producto() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
