package contabilidad.inventas.services;

import contabilidad.inventas.models.Producto;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public interface IProductoService
{
    List<Producto> findAll();

    Producto findById(String pID);

    Producto save(Producto producto);

    void delete(String pID);
}
