package contabilidad.inventas.rest;

import contabilidad.inventas.models.Producto;
import contabilidad.inventas.services.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/productos")
public class ProductoRestController
{
    @Autowired
    private IProductoService service;

    @GetMapping({"/","/index","/home","/list"})
    public List<Producto> index()
    {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Producto show(@PathVariable String id)
    {
        return service.findById( id );
    }

    @PostMapping("/")
    @ResponseStatus( HttpStatus.CREATED )
    public Producto create(@RequestBody Producto producto)
    {
        return service.save( producto );
    }

    @PutMapping("/{id}")
    @ResponseStatus( HttpStatus.CREATED )
    public Producto update(@RequestBody Producto producto,@PathVariable String id)
    {
        final Producto temp = service.findById( id );
        temp.setNombre( producto.getNombre() );
        temp.setPrecio( producto.getPrecio() );
        temp.setEstado( producto.isEstado() );
        return service.save( temp );
    }

    @DeleteMapping( "/clientes/{id}" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public void delete( @PathVariable String id )
    {
        service.delete( id );
    }
}
