package contabilidad.inventas.dao;

import contabilidad.inventas.models.Producto;
import org.springframework.data.repository.CrudRepository;

public interface IProductoDao extends CrudRepository<Producto,String>
{
}
